<?php

use Illuminate\Database\Seeder;
use Canoa\Persistence\Model\Veiculo;
use Ramsey\Uuid\Uuid;

class VeiculosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $veiculo = new Veiculo();
        $veiculo->uuid = Uuid::uuid4()->toString();
        $veiculo->veiculo = "Spin";
        $veiculo->marca = "General Motors";
        $veiculo->ano = 2012;
        $veiculo->descricao = "apenas testando pra ver";
        $veiculo->vendido = false;
        $veiculo->save();
        
        $veiculo = new Veiculo();
        $veiculo->uuid = Uuid::uuid4()->toString();
        $veiculo->veiculo = "Siena El 1.4";
        $veiculo->marca = "Fiat";
        $veiculo->ano = 2012;
        $veiculo->descricao = "apenas testando pra ver";
        $veiculo->vendido = true;
        $veiculo->save();
        
        $veiculo = new Veiculo();
        $veiculo->uuid = Uuid::uuid4()->toString();
        $veiculo->veiculo = "Pálio Celebration";
        $veiculo->marca = "Fiat";
        $veiculo->ano = 2008;
        $veiculo->descricao = "apenas testando pra ver";
        $veiculo->vendido = true;
        $veiculo->save();
        
        $veiculo = new Veiculo();
        $veiculo->uuid = Uuid::uuid4()->toString();
        $veiculo->veiculo = "HRV";
        $veiculo->marca = "Honda";
        $veiculo->ano = 2018;
        $veiculo->descricao = "apenas testando pra ver";
        $veiculo->vendido = false;
        $veiculo->save();
        
        $veiculo = new Veiculo();
        $veiculo->uuid = Uuid::uuid4()->toString();
        $veiculo->veiculo = "Hilux";
        $veiculo->marca = "Toyota";
        $veiculo->ano = 2010;
        $veiculo->descricao = "apenas testando pra ver";
        $veiculo->vendido = false;
        $veiculo->save();
        
    }
}
