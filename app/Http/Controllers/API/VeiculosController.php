<?php

namespace Canoa\Http\Controllers\API;

use Illuminate\Http\Request;
use Canoa\Http\Controllers\Controller;
use Canoa\Persistence\Repositories\VeiculosRepository;
use Canoa\Http\Requests\VeiculoRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Canoa\Helpers\JsonResponseBuilder;
use voku\helper\AntiXSS;

class VeiculosController extends Controller
{
    private $veiculosRepository;
    private $jsonBuilder;
    private $notFoundMessage;
    private $alreadyExistsMessage;

    public function __construct(VeiculosRepository $repository, JsonResponseBuilder $builder)
    {
        $this->veiculosRepository = $repository;
        $this->jsonBuilder = $builder;
        $this->notFoundMessage = "Veículo Não Encontrado";
        $this->alreadyExistsMessage = "Veículo Já Existe";
    }

    public function index(Request $request, AntiXSS $antiXSS)
    {
        $criterios = $request->except('page');

        //Higienização dos dados
        foreach ($criterios as $criterio => $valor) {
            $criterios[$criterio] = $antiXSS->xss_clean($valor);
        }

        if (count($criterios) > 0) {
            $dados = $this->veiculosRepository->getByCriteria($criterios);
            $json = $this->jsonBuilder->build($dados, 'paginacao');
            return response($json, 200)
                            ->header('Content-Type', 'application/json');
        } else {
            $dados = $this->veiculosRepository->getAll();
            $json = $this->jsonBuilder->build($dados, 'paginacao');
            return response($json, 200)
                            ->header('Content-Type', 'application/json');
        }
    }

    public function store(VeiculoRequest $request)
    {

        //Validação e Higienização dos dados na classe VeiculoRequest
        $dados = $request->all();

        
        $veiculo = $this->veiculosRepository->getByNaturalKey($dados['veiculo'], $dados['marca'], $dados['ano']);

        if (!$veiculo) {
            $dadosResposta = $this->veiculosRepository->save($dados);

            $json = $this->jsonBuilder->build($dadosResposta);

            return response($json, 201)
                            ->header('Content-Type', 'application/json');
        }
        
        $mensagem['content'] = $this->alreadyExistsMessage;
        $json = $this->jsonBuilder->build($mensagem, 'message');
        return response($json, 409)
                            ->header('Content-Type', 'application/json');
    }

    public function show(string $uuid)
    {
        try {
            $veiculo = $this->veiculosRepository->getById($uuid);
            $json = $this->jsonBuilder->build($veiculo);
            return response($json, 200)
                            ->header('Content-Type', 'application/json');
        } catch (ModelNotFoundException $ex) {
            $dados['content'] = $this->notFoundMessage;
            $json = $this->jsonBuilder->build($dados, 'message');
            return response($json, 404)
                            ->header('Content-Type', 'application/json');
            ;
        }
    }

    public function update(VeiculoRequest $request, string $uuid)
    {
        try {
            $dados = $request->all();
            $dados['uuid'] = $uuid;
            $this->veiculosRepository->update($dados);
            $mensagem = ["content" => "Veiculo Atualizado com Sucesso"];
            $json = $this->jsonBuilder->build($mensagem, 'mensagem');
            return response($json, 200)
                            ->header('Content-Type', 'application/json');
        } catch (ModelNotFoundException $ex) {
            $dados['content'] = $this->notFoundMessage;
            $json = $this->jsonBuilder->build($dados, 'message');
            return response($json, 404)
                            ->header('Content-Type', 'application/json');
            ;
        }
    }

    public function destroy($uuid)
    {
        try {
            $this->veiculosRepository->remove($uuid);
            $mensagem = ["content" => "Veiculo Removido com Sucesso"];
            $json = $this->jsonBuilder->build($mensagem, 'mensagem');
            return response($json, 200)
                            ->header('Content-Type', 'application/json');
        } catch (ModelNotFoundException $ex) {
            $dados['content'] = $this->notFoundMessage;
            $json = $this->jsonBuilder->build($dados, 'message');
            return response($json, 404)
                            ->header('Content-Type', 'application/json');
        }
    }
}
