<?php

namespace Canoa\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use voku\helper\AntiXSS;

class VeiculoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function prepareForValidation()
    {
        parent::prepareForValidation();
        $antiXSS = new AntiXSS();
       
        $dadosHigienizados = [];
        foreach ($this->all() as $indice => $valor) {
            $dadosHigienizados[$indice] = $antiXSS->xss_clean($valor);
            
            if (in_array($indice, ['veiculo', 'marca'])) {
                $dadosHigienizados[$indice] = \ucwords($valor);
            }
                
            //Normalizando
            $dadosHigienizados[$indice] = \preg_replace('/\s{2,}/', ' ', $valor);
        }
        
        
        $this->replace($dadosHigienizados);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'veiculo' => 'required',
            'marca' => 'required',
            'ano' => 'required|numeric|min:1900',
            'descricao' => 'required',
            'vendido' => 'required|boolean'
        ];
    }
}
