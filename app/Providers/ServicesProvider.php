<?php

namespace Canoa\Providers;

use Illuminate\Support\ServiceProvider;
use Canoa\Helpers\JsonResponseBuilder;
use Canoa\Helpers\JsonResponseBuilderDefault;

class ServicesProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(JsonResponseBuilder::class, JsonResponseBuilderDefault::class);
    }
}
