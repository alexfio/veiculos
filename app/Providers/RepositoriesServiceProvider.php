<?php

namespace Canoa\Providers;

use Illuminate\Support\ServiceProvider;
use Canoa\Persistence\Repositories\VeiculosRepository;
use Canoa\Persistence\Repositories\VeiculosEloquentRepository;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app
             ->bind(VeiculosRepository::class, VeiculosEloquentRepository::class);
    }
}
