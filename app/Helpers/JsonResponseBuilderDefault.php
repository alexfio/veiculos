<?php
namespace Canoa\Helpers;

use Canoa\Helpers\JsonResponseBuilder;

class JsonResponseBuilderDefault implements JsonResponseBuilder
{
    public function build(array $dados, string $type = 'data') : string
    {
        $resposta[$type] = $dados;
        return json_encode($resposta);
    }
}
