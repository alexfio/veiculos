<?php
namespace Canoa\Helpers;

interface JsonResponseBuilder
{
    public function build(array $dados, string $type = 'data') : string;
}
