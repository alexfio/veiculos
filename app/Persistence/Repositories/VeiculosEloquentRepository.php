<?php
namespace Canoa\Persistence\Repositories;

use Canoa\Persistence\Repositories\VeiculosRepository;
use Canoa\Persistence\Model\Veiculo;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class VeiculosEloquentRepository implements VeiculosRepository
{
    public function getAll(): array
    {
        return Veiculo::paginate(5)->toArray();
    }

    public function getById(string $uuid): array
    {
        return Veiculo::where('uuid', $uuid)->firstOrFail()
                                            ->toArray();
    }
    
    public function getByNaturalKey(string $veiculo, string $marca, int $ano)
    {
        $veiculo = Veiculo::where('veiculo', $veiculo)
                      ->where('marca', $marca)
                      ->where('ano', $ano)
                      ->first();
        
        return $veiculo ? $veiculo->toArray() : null;
    }
    
    public function getByCriteria(array $criterios): array
    {
        $veiculos = Veiculo::query();
        foreach ($criterios as  $criterio => $valor) {
            if (in_array($criterio, ['veiculo', 'marca'])) {
                $valor = '%' . \preg_replace('/\s+/', '%', $valor) . '%';
                
                $veiculos->where($criterio, 'like', $valor);
            } elseif (in_array($criterio, ['ano', 'vendido'])) {
                $veiculos->where($criterio, $valor);
            }
        }
        
        
        return $veiculos->paginate(5)->toArray();
    }
   
    public function save(array $dados): array
    {
        $veiculo = DB::transaction(function () use ($dados) {
            $veiculo = new Veiculo();
            $veiculo->uuid = Uuid::uuid4()->toString();
            $veiculo->veiculo = $dados['veiculo'];
            $veiculo->marca = $dados['marca'];
            $veiculo->ano = (integer) $dados['ano'];
            $veiculo->descricao = $dados['descricao'];
            $veiculo->vendido = $dados['vendido'];
        
            $veiculo->save();
            
            return $veiculo->toArray();
        });
        
        return $veiculo;
    }

    public function update(array $dados): array
    {
        $veiculo = DB::transaction(function () use ($dados) {
            $veiculo = Veiculo::where('uuid', $dados['uuid'])->firstOrFail();
            $veiculo->veiculo = $dados['veiculo'];
            $veiculo->marca = $dados['marca'];
            $veiculo->ano = (integer) $dados['ano'];
            $veiculo->descricao = $dados['descricao'];
            $veiculo->vendido = $dados['vendido'];
        
            $veiculo->save();
            
            return $veiculo->toArray();
        });
        
        return $veiculo;
    }

    
    public function remove(string $uuid): void
    {
        DB::transaction(function () use ($uuid) {
            $veiculo = Veiculo::where('uuid', $uuid)->firstOrFail();
            $veiculo->delete();
        });
    }
}
