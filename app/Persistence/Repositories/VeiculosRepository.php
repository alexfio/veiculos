<?php
namespace Canoa\Persistence\Repositories;

interface VeiculosRepository
{
    public function getAll() : array;
    public function getById(string $uuid) : array;
    public function getByNaturalKey(string $veiculo, string $marca, int $ano);
    public function getByCriteria(array $criterios): array;
    public function save(array $dados) : array;
    public function update(array $dados): array;
    public function remove(string $uuid) : void;
}
