<?php

namespace Canoa\Persistence\Model;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    protected $dates = ['created_at', 'updated_at'];
}
