
function HomeController(veiculos, VeiculosService, $location, $window) {
    var self = this;

    self.buscarVeiculos = buscarVeiculos;
    self.resetarBusca = resetarBusca;
    self.removerVeiculo = removerVeiculo;

    self.paginacao = veiculos.paginacao;
    self.paginaAtual = self.paginacao.current_page;
    self.veiculos = self.paginacao.data;
    self.paginas = [];
    self.total_paginas = Math.ceil(self.paginacao.total / self.paginacao.per_page);

    var criterios = $location.search();
    delete criterios.page;

    var queryString = Object.keys(criterios).map(function (key) {
        return key + '=' + criterios[key]
    }).join('&');

    queryString = encodeURI(queryString);


    for (i = 1; i <= self.total_paginas; i++) {
        var pagina = {};
        pagina.numero = i;
        pagina.query = 'page=' + i + '&' + queryString;
        self.paginas.push(pagina);
    }

    function resetarBusca() {
        $window.location.href = "index.html";
    }

    function removerVeiculo(veiculo) {
        
        var respostaRemocao = confirm('Deseja remover o veiculo ' + veiculo.veiculo + ' ?');

        if(respostaRemocao) 
            VeiculosService.removeVeiculo(veiculo.uuid)
                .then(removeSuccess, removeError);

        function removeSuccess(resposta) {
            self.veiculos = self.veiculos.filter(function (valor) {
                return valor.uuid !== veiculo.uuid;
                indice = index;
            }, veiculo.uuid);
        }

        function removeError(resposta) {
            alert('Falha ao remover o veiculo.');
        }

    }

    function buscarVeiculos(criterios) {

        VeiculosService.getAll(criterios)
                .then(getAllSuccess, getAllError);

        function getAllSuccess(resposta) {

            self.paginacao = resposta.paginacao;
            self.paginaAtual = resposta.paginacao.current_page;
            self.veiculos = resposta.paginacao.data;
            self.queryString = VeiculosService.queryString;

            self.paginas = [];
            self.total_paginas = Math.ceil(self.paginacao.total / self.paginacao.per_page);
            for (i = 1; i <= self.total_paginas; i++) {
                var pagina = {};
                pagina.numero = i;
                pagina.query = 'page=' + i + '&' + self.queryString;
                self.paginas.push(pagina);
            }

        }

        function getAllError(resposta) {
            alert('Falha ao obter os dados.');
        }

    }

}




