
function EditarVeiculoController(veiculo, VeiculosService, $location) {
    var self = this;

    self.veiculo = veiculo.data || {};
    self.editar = editar;
    
    

    function editar(veiculo) {

        VeiculosService.editarVeiculo(veiculo)
                       .then(editarSuccess, editarError);
     
        function editarSuccess(resposta) {
            alert("Dados do Veiculo atualizados com sucesso !");
            delete self.veiculo;
            $location.path('/home');
        }

        function editarError(resposta) {
            alert("Falha ao atualizar os dados do veiculo !");
        }
    }

}




