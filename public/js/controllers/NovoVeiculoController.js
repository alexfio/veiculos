
function NovoVeiculoController($location, VeiculosService) {
    var self = this;

    self.veiculo = {};
    self.cadastrar = cadastrar;

    function cadastrar(veiculo) {

        VeiculosService.cadastrarVeiculo(veiculo)
                       .then(cadastrarSuccess, cadastrarError);
     
        function cadastrarSuccess(resposta) {
            alert("Veiculo cadastrado com sucesso !");
            delete self.veiculo;
            $location.path('/home');
        }

        function cadastrarError(resposta) {
            alert(resposta.data.message.content + "!");
        }
    }

}




