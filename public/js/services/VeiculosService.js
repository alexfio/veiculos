
function VeiculosService($http) {

    let self = this;
    self.getAll = getAll;
    self.getById = getById;
    self.editarVeiculo = editarVeiculo;
    self.removeVeiculo = removeVeiculo;
    self.cadastrarVeiculo = cadastrarVeiculo;
    
    self.queryString = '';

    function getById(uuid) {
        return $http.get('/api/veiculos/' + uuid);
              
    }

    function getAll(criterios) {
        criterios = criterios || { page: 1 };
        var queryString = Object.keys(criterios).map(function (key) {
            return key + '=' + criterios[key]
        }).join('&');
        
        queryString = encodeURI(queryString);
        self.queryString = queryString;
        
        return $http.get('/api/veiculos?' + queryString)
                .then(success, error);
    }
    
    function removeVeiculo(uuid) {
        
        return $http.delete('/api/veiculos/' + uuid)
                .then(success, error);
    }
    
    function cadastrarVeiculo(veiculo) {
        return $http.post('/api/veiculos', veiculo);
                   
    }
    
    function editarVeiculo(veiculo) {
        return $http.put('/api/veiculos/' + veiculo.uuid, veiculo);
                   
    }
    
  
}

function success(resposta) {
    return resposta.data;
}

function error(data) {
    return data;
}
