(function () {
    let app = angular.module('veiculos', ['ngRoute']);
    app.config(["$routeProvider", configurarRotas])
            .service('VeiculosService', ['$http', VeiculosService])
            .controller('HomeController', [
                'veiculos',
                'VeiculosService',
                '$location',
                '$window',
                HomeController
            ]
                    ).controller('NovoVeiculoController', ["$location", "VeiculosService", NovoVeiculoController])
            .controller('EditarVeiculoController', ["veiculo", 'VeiculosService', '$location', EditarVeiculoController])
            .controller('VerDetalhesVeiculoController', ["veiculo", VerDetalhesVeiculoController]);

})();


function configurarRotas($routeProvider) {
    $routeProvider
            .when('/home', {

                controller: 'HomeController',
                templateUrl: './views/home.html',
                controllerAs: 'ctrl',
                resolve: {
                    veiculos: ['VeiculosService', '$location',
                        function (veiculosService, $location) {

                            var criterios = $location.search();

                            return veiculosService.getAll(criterios);
                        }]
                }

            }).when('/novo-veiculo', {
        controller: 'NovoVeiculoController',
        templateUrl: './views/novo-veiculo.html',
        controllerAs: 'ctrl'

    }).when('/editar-veiculo/:uuid', {
        controller: 'EditarVeiculoController',
        templateUrl: './views/editar-veiculo.html',
        controllerAs: 'ctrl',

        resolve: {
            veiculo: ['VeiculosService', '$route','$location','$q',
                function (veiculosService, $route, $location, $q) {
                    var uuid = $route.current.params.uuid;

                    return veiculosService.getById(uuid).then(function (resposta) {
                        return resposta.data;
                    },
                            function (resposta) {
                                $location.path('/home');
                                return $q.reject();
                            });
                    ;
                }]
        }

    }).when('/ver-detalhes/:uuid', {
        controller: 'VerDetalhesVeiculoController',
        templateUrl: './views/ver-detalhes.html',
        controllerAs: 'ctrl',

        resolve: {
            veiculo: ['VeiculosService', '$route', '$location','$q',
                function (veiculosService, $route, $location) {
                    var uuid = $route.current.params.uuid;

                    return veiculosService.getById(uuid)
                            .then(function (resposta) {
                                return resposta.data;
                            },
                                    function (resposta) {
                                        $location.path('/home');
                                        return $q.reject();

                                    });
                }]
        }

    }).otherwise({
        redirectTo: '/home'
    });
}