# Canoa Veículos
**Francisco Chagas Alexandre de Souza Filho**

- O presente código é uma implementação de uma API restful para cadastro,
edição, visualização e exclusão de uma base de veículos. 
- Assim como uma interface para consumo desta API.
- Como SGBD relacional foi usado o MySQL, no front-end Bootstrap 4.0 e AngularJS. Além disso
foi usado PHP 7.2 e Laravel.

**Requisitos técnicos:**

 - Desenvolver uma API JSON RESTful em PHP, que utilize todos os métodos (GET, POST, PUT, DELETE).

**Requisitos funcionais :**

 - Interface deve conter:
    - Listagem de veículos
    - Visualizar detalhes do veículo
    - Formulário para criar veículo
    - Formulário para editar veículo
    - Exclusão de veículo



# Instruções de Instalação

- Para uma maior facilidade de execução do projeto, o arquivo **.env**
foi adicionado ao git. Esse arquivo já está preparado para o uso com o Docker, **caso queira executar o sistema sem usar o Docker,
altere as configurações do arquivo para os valores adequados, principalmente o endereço de Host do Mysql, bem como
usuário e senha.**
         

O projeto pode ser executado com Docker ou sem o Docker.

   - **Com o Docker**:
	   -  Na raiz do projeto execute:
		   - docker-compose up -d
	   - Instalar as dependências do php via composer: 
		   - docker exec -it web composer install
	   - Instalar as dependências de front-end via bower:
		   - docker exec -it web bower --allow-root install
	   - Rodar as migrations:
		   - docker exec -it web php artisan **migrate** 
	  - No navegador digite: **localhost** para acessar o projeto 
          - Para destruir os containers após a execução da aplicação faça: 
             - docker-compose down
	  
- **Sem o Docker**:
    - É necessário que os seguintes softwares estejam instalados:
       - **Composer**, **Bower**, **PHP 7.2** e **MySQL 5.7**.
    
    - No mysql **crie um novo banco de dados** chamado veiculos
                   
    - Na raiz do projero execute
	     -  composer install
	     -  bower  install
	     -  Rodar as migrations e os seeders:
		   	- php artisan **migrate** 
	     -  php artisan serve
	- No navegador digite: **localhost:8000** para acessar o projeto .
		   
	